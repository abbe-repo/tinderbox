
$FreeBSD: ports/textproc/htmltolatex/files/patch-h2l.l,v 1.2 2003/02/21 22:17:34 naddy Exp $

--- h2l.l.orig	Wed Oct 10 14:24:26 2001
+++ h2l.l	Fri Feb 21 03:10:30 2003
@@ -1,8 +1,10 @@
 %{
+using namespace std;
+
 #include "CLexData.h"
 #define YYSTYPE	CLexDataPtr
 
-#include "h2l.tab.cpp.h"
+#include "h2l.tab.hpp"
 #include "CH2L.h"
 #include "util.h"
 #include <assert.h>
