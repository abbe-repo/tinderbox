#!/bin/sh
#
# $FreeBSD: ports/textproc/jaxup/files/jaxup-xupdater.sh,v 1.2 2011/07/21 05:02:57 linimon Exp $

JAVA_VERSION="%%JAVA_VERSION%%" %%LOCALBASE%%/bin/java -cp "`"%%LOCALBASE%%/bin/classpath"`:%%DATADIR%%/jaxup-xupdater.jar" "DOMXUpdater" "$@"
