#!/bin/sh
# $FreeBSD: ports/biology/babel/files/babel.sh,v 1.1 2001/12/02 13:09:47 naddy Exp $

export BABEL_DIR=${BABEL_DIR-@PREFIX@/share/babel}

exec @PREFIX@/libexec/babel "$@"
