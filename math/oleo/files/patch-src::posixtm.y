$FreeBSD: ports/math/oleo/files/patch-src::posixtm.y,v 1.1 2003/01/02 16:33:58 petef Exp $

--- src/posixtm.y.orig	Thu Jan  2 11:29:14 2003
+++ src/posixtm.y	Thu Jan  2 11:30:01 2003
@@ -85,6 +85,7 @@
 		   YYABORT;
 		 }
 	       }
+	   ;
 
 year : digitpair {
                    t.tm_year = $1;
