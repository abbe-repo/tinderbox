
$FreeBSD$

--- src/netdev.c.orig
+++ src/netdev.c
@@ -309,6 +309,7 @@
 	if ((sock=socket(PF_INET,SOCK_DGRAM, IPPROTO_UDP))==-1)
 		return 0;
 	if (ioctl(sock,SIOCGIFCONF,&ifc)==-1) {
+		close(sock);
 	        return 0;
 	}
 	ad=buf;
@@ -320,6 +321,7 @@
 		if (run_ipv4) {
 			if (ir->ifr_addr.sa_family==AF_INET &&
 			    ((struct sockaddr_in *)&ir->ifr_addr)->sin_addr.s_addr==a->ipv4.s_addr) {
+				close(sock);
 				return 1;
 			}
 		}
@@ -328,6 +330,7 @@
 		ELSE_IPV6 {
 			if (ir->ifr_addr.sa_family==AF_INET6 &&
 			    IN6_ARE_ADDR_EQUAL(&((struct sockaddr_in6 *)&ir->ifr_addr)->sin6_addr,&a->ipv6)) {
+				close(sock);
 				return 1;
 			}
 		}
