
$FreeBSD: ports/editors/p5-Padre/files/patch-lib_Padre_Wx.pm,v 1.1 2009/09/20 20:14:24 pav Exp $

--- lib/Padre/Wx.pm.orig
+++ lib/Padre/Wx.pm
@@ -67,6 +67,7 @@
 
 sub launch_browser {
 	require Padre::Task::LaunchDefaultBrowser;
+	Wx::LaunchDefaultBrowser( $_[0] ); return 1;
 	Padre::Task::LaunchDefaultBrowser->new(
 		url => $_[0],
 	)->schedule;
