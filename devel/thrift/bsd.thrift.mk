#
# $FreeBSD: ports/devel/thrift/bsd.thrift.mk,v 1.1 2012/01/06 01:02:56 scheidell Exp $
#
# to use:
# in your makefile, set:
# PORTVERSION=	${THRIFT_PORTVERSION}
# see $PORTSDIR/devel/thrift for examples 
THRIFT_PORTVERSION=	0.8.0
