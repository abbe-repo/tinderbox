
$FreeBSD: ports/devel/codeworker/files/patch-GrfForeach.h,v 1.1 2007/05/26 21:01:13 krion Exp $

--- GrfForeach.h.orig
+++ GrfForeach.h
@@ -83,7 +83,7 @@
 		virtual SEQUENCE_INTERRUPTION_LIST executeReverseSortedForeach(DtaScriptVariable& theVariable, DtaScriptVariable& stackForeach);
 		virtual SEQUENCE_INTERRUPTION_LIST executeReverseSortedIndirectForeach(DtaScriptVariable& theVariable, DtaScriptVariable& stackForeach);
 
-		SEQUENCE_INTERRUPTION_LIST GrfForeach::iterate(DtaArrayIterator& iteratorData, DtaScriptVariable& stackForeach);
+		SEQUENCE_INTERRUPTION_LIST iterate(DtaArrayIterator& iteratorData, DtaScriptVariable& stackForeach);
 	};
 }
 
