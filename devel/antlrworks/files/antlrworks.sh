#! /bin/sh
#
# $FreeBSD: ports/devel/antlrworks/files/antlrworks.sh,v 1.1 2011/06/17 07:19:15 amdmi3 Exp $

JAVA_VERSION="%%JAVA_VERSION%%" "%%LOCALBASE%%/bin/java" -classpath "%%JAVAJARDIR%%/%%PORTNAME%%.jar" org.antlr.works.IDE "$@"
 
