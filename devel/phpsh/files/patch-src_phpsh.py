
$FreeBSD: ports/devel/phpsh/files/patch-src_phpsh.py,v 1.1 2011/08/23 17:44:59 ashish Exp $

--- src/phpsh.py.orig
+++ src/phpsh.py
@@ -274,7 +274,7 @@
         self.config.add_section("Emacs")
 
     def read(self):
-        config_files = ["/etc/phpsh/config"]
+        config_files = ["%%PREFIX%%/etc/phpsh/config"]
         home = os.getenv("HOME")
         if home:
             homestr = home.strip()
