# $FreeBSD: ports/devel/subversion16/Makefile.inc,v 1.2 2012/02/14 22:50:59 scheidell Exp $
# this keeps subversion16 and ../svnmerge in sync, see pr 164854

PORTVERSION=	1.6.17
