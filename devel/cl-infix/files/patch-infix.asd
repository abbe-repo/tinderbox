
$FreeBSD: ports/devel/cl-infix/files/patch-infix.asd,v 1.1 2010/10/20 22:36:14 olgeni Exp $

--- infix.asd.orig
+++ infix.asd
@@ -16,4 +16,4 @@
 	       (:static-file "COPYING")))
 
 (defmethod source-file-type ((f cl-source-file) (s (eql (find-system 'infix))))
-  "cl")
+  "lisp")
