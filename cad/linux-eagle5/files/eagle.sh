#!/bin/sh
#
# $FreeBSD: ports/cad/linux-eagle5/files/eagle.sh,v 1.2 2009/08/03 09:45:20 bsam Exp $

EAGLEBIN=%%DATADIR%%/bin/eagle

$EAGLEBIN "$@"
