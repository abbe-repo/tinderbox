
$FreeBSD: ports/net-mgmt/ndpmon/files/patch-parser.c,v 1.3 2010/05/30 23:55:30 sylvio Exp $

--- parser.c.orig
+++ parser.c
@@ -77,8 +77,8 @@
 		write_proc("/proc/sys/net/ipv6/conf/all/accept_ra_defrtr",flag);
 		write_proc("/proc/sys/net/ipv6/conf/all/accept_ra_pinfo",flag);
 		write_proc("/proc/sys/net/ipv6/conf/all/accept_redirects",flag);
-	}
 #endif
+	}
 	xmlXPathFreeObject (xmlobject);
 	return;
 }
