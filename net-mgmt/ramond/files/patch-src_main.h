
$FreeBSD: ports/net-mgmt/ramond/files/patch-src_main.h,v 1.1 2011/09/25 16:10:51 zi Exp $

--- src/main.h.orig
+++ src/main.h
@@ -1,5 +1,6 @@
 #include <stdlib.h>
 #include <stdio.h>
+#include <fcntl.h>
 #include <errno.h>
 #include <unistd.h>
 #include <time.h>