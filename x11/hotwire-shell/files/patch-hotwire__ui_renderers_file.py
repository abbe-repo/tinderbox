
$FreeBSD: ports/x11/hotwire-shell/files/patch-hotwire__ui_renderers_file.py,v 1.1 2008/06/30 00:25:09 lx Exp $

--- hotwire_ui/renderers/file.py.orig
+++ hotwire_ui/renderers/file.py
@@ -245,7 +245,10 @@
                 menuitem = gtk.ImageMenuItem(_('Edit with %s') % (prefeditor.name,))
                 menuitem.connect('activate', self.__on_edit_activated, self.context.get_cwd(), prefeditor, fobj.path)
                 pbcache = PixbufCache.getInstance()
-                pixbuf = pbcache.get(prefeditor.icon, size=16, trystock=True, stocksize=gtk.ICON_SIZE_MENU)
+		if prefeditor.icon:
+	                pixbuf = pbcache.get(prefeditor.icon, size=16, trystock=True, stocksize=gtk.ICON_SIZE_MENU)
+		else:
+			pixbuf = None
                 if pixbuf:
                     img = gtk.image_new_from_pixbuf(pixbuf)
                     menuitem.set_property('image', img)
