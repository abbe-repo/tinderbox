
$FreeBSD: ports/audio/epos-devel/files/patch-src_epos.h,v 1.1 2005/10/19 16:11:30 vs Exp $

--- src/epos.h.orig
+++ src/epos.h
@@ -43,6 +43,7 @@
 		#include <rx.h>
 	#else
 	    #ifdef HAVE_REGEX_H
+		#include <sys/types.h>
 		#include <regex.h>
 	    #else
 		#include "rx.h"
