
$FreeBSD: ports/audio/pms/files/patch-src_pms.cpp,v 1.1 2010/05/19 10:42:54 wen Exp $

--- src/pms.cpp.orig
+++ src/pms.cpp
@@ -23,6 +23,10 @@
 
 #include "pms.h"
 
+#ifdef __FreeBSD__
+#include <sys/wait.h>
+#endif
+
 using namespace std;
 
 Pms *		pms;
