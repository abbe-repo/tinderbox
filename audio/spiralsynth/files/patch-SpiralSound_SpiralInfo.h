$FreeBSD: ports/audio/spiralsynth/files/patch-SpiralSound_SpiralInfo.h,v 1.1 2004/02/09 13:55:42 trevor Exp $

--- SpiralSound/SpiralInfo.h.orig	Sun Nov 26 15:52:46 2000
+++ SpiralSound/SpiralInfo.h	Mon Feb  9 13:31:38 2004
@@ -19,6 +19,7 @@
 #include <iostream.h>
 #include <string>
 #include <stdlib.h>
+using namespace std;
 
 #ifndef SpiralINFO
 #define SpiralINFO
