
$FreeBSD: ports/www/p5-HTTP-Recorder/files/patch-lib_HTTP_Recorder.pm,v 1.1 2009/10/05 11:24:31 tobez Exp $

--- lib/HTTP/Recorder.pm.orig
+++ lib/HTTP/Recorder.pm
@@ -344,12 +344,13 @@
     # get rid of the arguments we added
     my $prefix = $self->{prefix};
 
-    for my $key ($content->query_param) {
-	if ($key =~ /^$prefix-/) {
-	    $content->query_param_delete($key);
-	}
-    }
-    return $content;
+	$content =~ s/$prefix-(.*?)\?(.*?)&//g;
+	$content =~ s/$prefix-(.*?)&//g;
+	$content =~ s/$prefix-(.*?)$//g;
+	$content =~ s/&$//g;
+	$content =~ s/\?$//g;
+
+	return $content;
 }
 
 sub extract_values {
