
$FreeBSD: ports/games/heretic/files/patch-graphics::i_sdl_gl.c,v 1.1 2002/04/17 17:58:38 sobomax Exp $

--- graphics/i_sdl_gl.c	2002/04/17 17:57:00	1.1
+++ graphics/i_sdl_gl.c	2002/04/17 17:57:14
@@ -4,7 +4,7 @@
 #include <sys/time.h>
 #include "doomdef.h"
 
-#include <SDL/SDL.h>
+#include <SDL.h>
 
 #include "gl_struct.h"
 
