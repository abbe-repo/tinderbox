#
# $FreeBSD: ports/lang/ghc/bsd.cabal.options.mk,v 1.1 2010/05/22 22:41:48 pgj Exp $
#
# bsd.cabal.options.mk -- Support options for ports based on Haskell Cabal.
#
# Created by: Gabor Pali <pgj@FreeBSD.org>
#
# Maintained by: haskell@FreeBSD.org
#

PKGNAMEPREFIX?=	hs-
