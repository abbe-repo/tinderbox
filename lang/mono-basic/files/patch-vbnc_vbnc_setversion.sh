
$FreeBSD: ports/lang/mono-basic/files/patch-vbnc_vbnc_setversion.sh,v 1.1 2011/06/12 19:06:23 romain Exp $

--- vbnc/vbnc/setversion.sh.orig
+++ vbnc/vbnc/setversion.sh
@@ -1,4 +1,4 @@
-#!/bin/bash -ex
+#!/usr/bin/env bash -ex
 
 VERSION_VB=$1
 VERSION_TMP=version.tmp
