
$FreeBSD: ports/security/openscep/files/patch-lib_fingerprint.c,v 1.1 2006/08/12 11:05:37 simon Exp $

--- lib/fingerprint.c.orig
+++ lib/fingerprint.c
@@ -9,6 +9,7 @@
 #include <fingerprint.h>
 #include <openssl/bio.h>
 #include <openssl/evp.h>
+#include <openssl/md5.h>
 #include <init.h>
 #include <string.h>
 
