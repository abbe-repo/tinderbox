
$FreeBSD: ports/security/openscep/files/patch-lib_proxy.c,v 1.1 2006/08/12 11:05:37 simon Exp $

--- lib/proxy.c.orig
+++ lib/proxy.c
@@ -10,6 +10,7 @@
 #include <scep.h>
 #include <proxy.h>
 #include <openssl/evp.h>
+#include <openssl/md5.h>
 
 /*
  * proxy_authenticator	compute the proxy authenticator hash value
