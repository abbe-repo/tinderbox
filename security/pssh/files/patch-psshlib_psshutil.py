
$FreeBSD: ports/security/pssh/files/patch-psshlib_psshutil.py,v 1.4 2011/02/11 18:12:28 brooks Exp $

--- psshlib/psshutil.py.orig
+++ psshlib/psshutil.py
@@ -27,7 +27,10 @@
     Returns a list of (host, port, user) triples.
     """
     lines = []
-    f = open(path)
+    if path == "-":
+        f = open("/dev/stdin")
+    else:
+        f = open(path)
     for line in f:
         lines.append(line.strip())
     f.close()
