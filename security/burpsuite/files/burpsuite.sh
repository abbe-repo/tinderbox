#!/bin/sh
#
# $FreeBSD: ports/security/burpsuite/files/burpsuite.sh,v 1.1 2010/03/08 22:03:20 niels Exp $
#

cd %%PATH%%
exec %%JAVA_CMD%% -jar -Xmx256m %%BURP%% "$@"
