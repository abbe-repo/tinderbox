#!/bin/sh
#
# $FreeBSD: ports/security/dirbuster/files/dirbuster.sh,v 1.1 2010/03/09 21:52:06 niels Exp $
#

cd %%PATH%%
exec %%JAVA_CMD%% -jar -Xmx256m %%DIRBUSTER%% "$@"
