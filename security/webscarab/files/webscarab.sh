#!/bin/sh
#
# $FreeBSD: ports/security/webscarab/files/webscarab.sh,v 1.1 2010/03/10 22:07:09 niels Exp $
#

TEST=`/usr/bin/basename $0`
OPTIONS="-DWebScarab.lite=false"

if [ "${TEST}" = "webscarab_lite" ]; then
	OPTIONS=""
fi


cd %%PATH%%
exec %%JAVA_CMD%% ${OPTIONS} -jar -Xmx256m %%WEBSCARAB%% "$@"
