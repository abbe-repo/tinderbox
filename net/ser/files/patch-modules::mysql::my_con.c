
$FreeBSD: ports/net/ser/files/patch-modules::mysql::my_con.c,v 1.1 2006/04/21 06:30:43 sobomax Exp $

--- modules/mysql/my_con.c
+++ modules/mysql/my_con.c
@@ -70,6 +70,9 @@
 		goto err;
 	}
 
+	/* Enable reconnection explictly */
+	ptr->con->reconnect = 1;
+
 	ptr->timestamp = time(0);
 
 	ptr->id = id;
