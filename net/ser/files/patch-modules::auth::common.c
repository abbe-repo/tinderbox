
$FreeBSD: ports/net/ser/files/patch-modules::auth::common.c,v 1.1 2005/07/07 20:16:35 sobomax Exp $

--- modules/auth/common.c.orig
+++ modules/auth/common.c
@@ -95,5 +95,5 @@
 		}
 	}
 
-	return sl_reply(_m, (char*)(long)_code, _reason);
+	return (use_tm != 0) ? tmb.t_reply(_m, _code, _reason) : sl_reply(_m, (char*)(long)_code, _reason);
 }
