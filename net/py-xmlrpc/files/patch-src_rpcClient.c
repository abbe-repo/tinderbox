
$FreeBSD: ports/net/py-xmlrpc/files/patch-src_rpcClient.c,v 1.1 2009/02/27 01:35:30 sobomax Exp $

--- src/rpcClient.c
+++ src/rpcClient.c
@@ -179,7 +179,7 @@
 	cp->url = NULL;
 	Py_DECREF(cp->src);
 	Py_DECREF(cp->disp);
-	PyMem_DEL(cp);
+	PyObject_DEL(cp);
 }
 
 
