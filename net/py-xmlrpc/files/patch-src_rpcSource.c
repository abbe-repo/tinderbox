
$FreeBSD: ports/net/py-xmlrpc/files/patch-src_rpcSource.c,v 1.1 2009/02/27 01:35:30 sobomax Exp $

--- src/rpcSource.c
+++ src/rpcSource.c
@@ -61,7 +61,7 @@
 	if (srcp->onErr and srcp->onErrType == ONERR_TYPE_PY) {
 		Py_DECREF((PyObject *)srcp->onErr);
 	}
-	PyMem_DEL(srcp);
+	PyObject_DEL(srcp);
 }
 
 
