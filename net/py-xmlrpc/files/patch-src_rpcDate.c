
$FreeBSD: ports/net/py-xmlrpc/files/patch-src_rpcDate.c,v 1.1 2009/02/27 01:35:30 sobomax Exp $

--- src/rpcDate.c
+++ src/rpcDate.c
@@ -75,7 +75,7 @@
 	if (dp->value) {
 		Py_DECREF(dp->value);
 	}
-	PyMem_DEL(dp);
+	PyObject_DEL(dp);
 }
 
 
