
$FreeBSD: ports/net/py-xmlrpc/files/patch-src_rpcDispatch.c,v 1.1 2009/02/27 01:35:30 sobomax Exp $

--- src/rpcDispatch.c
+++ src/rpcDispatch.c
@@ -68,7 +68,7 @@
 		rpcDispClear(dp);
 		free(dp->srcs);
 	}
-	PyMem_DEL(dp);
+	PyObject_DEL(dp);
 }
 
 
