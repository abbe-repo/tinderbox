$FreeBSD: ports/net/dgd/files/patch-src-kfun-std.c,v 1.2 2011/08/03 06:31:02 glewis Exp $

--- src/kfun/std.c.orig	2011-07-30 00:30:37.000000000 -0700
+++ src/kfun/std.c	2011-07-30 00:30:49.000000000 -0700
@@ -1291,10 +1291,6 @@
 	error("open_port() in special purpose object");
     }
 
-    if (obj->flags & O_DRIVER) {
-	error("open_port() in driver object");
-    }
-
     if (f->level != 0) {
 	error("open_port() within atomic function");
     }
