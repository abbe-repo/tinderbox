
$FreeBSD: ports/x11-fonts/gfe/files/patch-src_bdf.c,v 1.1 2002/11/30 01:09:55 naddy Exp $

--- src/bdf.c.orig	Tue Jun 27 12:36:09 2000
+++ src/bdf.c	Sat Nov 30 02:04:14 2002
@@ -18,7 +18,8 @@
 #include <gtk/gtk.h>
 
 #include <ctype.h>
-#include <malloc.h>
+#include <stdlib.h>
+#include <string.h>
 
 #include "auxil.h"
 #include "bdf.h"
