
$FreeBSD: ports/multimedia/moonlight/files/patch-plugin_plugin.cpp,v 1.1 2010/06/01 12:41:28 romain Exp $

--- plugin/plugin.cpp.orig
+++ plugin/plugin.cpp
@@ -17,6 +17,7 @@
 #include <fcntl.h>
 #include <stdlib.h>
 #include <dlfcn.h>
+#include <unistd.h>
 
 #include "plugin.h"
 #include "plugin-spinner.h"
