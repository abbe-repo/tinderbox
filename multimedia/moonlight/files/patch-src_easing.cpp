
$FreeBSD: ports/multimedia/moonlight/files/patch-src_easing.cpp,v 1.1 2010/06/01 12:41:28 romain Exp $

--- src/easing.cpp.orig
+++ src/easing.cpp
@@ -16,7 +16,9 @@
 #include <stdio.h>
 #include <stdlib.h>
 #include <string.h>
+#ifndef __FreeBSD__
 #include <malloc.h>
+#endif
 #include <math.h>
 
 #include <runtime.h>
