
$FreeBSD: ports/multimedia/moonlight/files/patch-src_bitmapimage.cpp,v 1.1 2010/06/01 12:41:28 romain Exp $

--- src/bitmapimage.cpp.orig
+++ src/bitmapimage.cpp
@@ -16,6 +16,9 @@
 #include <glib/gstdio.h>
 #include <fcntl.h>
 #include <errno.h>
+#ifdef __FreeBSD__
+#include <unistd.h>
+#endif
 
 #include "application.h"
 #include "bitmapimage.h"
