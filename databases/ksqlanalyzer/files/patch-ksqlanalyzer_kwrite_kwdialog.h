
$FreeBSD: ports/databases/ksqlanalyzer/files/patch-ksqlanalyzer_kwrite_kwdialog.h,v 1.1 2007/04/06 13:36:35 mich Exp $

--- ksqlanalyzer/kwrite/kwdialog.h.orig
+++ ksqlanalyzer/kwrite/kwdialog.h
@@ -13,6 +13,8 @@
 
 #include "kwview.h"
 
+class QComboBox;
+
 class SearchDialog : public QDialog {
     Q_OBJECT
   public:
