
$FreeBSD: ports/irc/ctrlproxy/files/patch-lib_listener.h,v 1.1 2009/03/01 23:40:05 miwi Exp $

--- lib/listener.h.orig
+++ lib/listener.h
@@ -5,8 +5,12 @@
 #include "ctrlproxy.h"
 
 #ifdef HAVE_GSSAPI
+#if (__FreeBSD__ >= 7)
+#include <gssapi/gssapi.h>
+#else
 #include <gssapi.h>
 #endif
+#endif
 
 #ifndef G_MODULE_EXPORT
 #define G_MODULE_EXPORT
