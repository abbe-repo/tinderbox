
$FreeBSD: ports/archivers/star/files/patch-rmt_Makefile.man,v 1.1 2009/02/07 20:44:18 olgeni Exp $

--- rmt/Makefile.man.orig
+++ rmt/Makefile.man
@@ -8,10 +8,10 @@
 ###########################################################################
 
 MANDIR=		man
-TARGETMAN=	rmt
+TARGETMAN=	srmt
 MANSECT=	$(MANSECT_CMD)
 MANSUFFIX=	$(MANSUFF_CMD)
-MANFILE=	rmt.1
+MANFILE=	srmt.1
 
 ###########################################################################
 include		$(SRCROOT)/$(RULESDIR)/rules.man
