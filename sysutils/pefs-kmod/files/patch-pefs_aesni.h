--- sys/fs/pefs/pefs_aesni.h.orig
+++ sys/fs/pefs/pefs_aesni.h
@@ -26,6 +26,8 @@
  * $FreeBSD: ports/sysutils/pefs-kmod/files/patch-pefs_aesni.h,v 1.2 2011/09/06 10:53:06 pav Exp $
  */
 
+#ifdef PEFS_AESNI
+
 #include <crypto/aesni/aesni.h>
 
 struct pefs_aesni_ctx {
@@ -41,3 +43,5 @@ struct pefs_aesni_ses {
 };
 
 algop_init_t	pefs_aesni_init;
+
+#endif
