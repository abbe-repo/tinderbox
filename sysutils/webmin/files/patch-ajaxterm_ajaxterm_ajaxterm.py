
$FreeBSD: ports/sysutils/webmin/files/patch-ajaxterm_ajaxterm_ajaxterm.py,v 1.1 2012/02/13 13:24:13 olgeni Exp $

--- ajaxterm/ajaxterm/ajaxterm.py.orig
+++ ajaxterm/ajaxterm/ajaxterm.py
@@ -394,7 +394,7 @@
 			if self.cmd:
 				cmd=['/bin/sh','-c',self.cmd]
 			elif os.getuid()==0:
-				cmd=['/bin/login']
+				cmd=['/usr/bin/login']
 			else:
 				sys.stdout.write("Login: ")
 				login=sys.stdin.readline().strip()
