
$FreeBSD: ports/sysutils/sge62/files/patch-clients_common_sge__client__ijs.c,v 1.2 2008/12/15 20:45:44 brooks Exp $

--- clients/common/sge_client_ijs.c.orig
+++ clients/common/sge_client_ijs.c
@@ -36,7 +36,7 @@
 #include <signal.h>
 #include <string.h>
 
-#if defined(DARWIN)
+#if defined(DARWIN) || defined(FREEBSD)
 #  include <termios.h>
 #  include <sys/ttycom.h>
 #  include <sys/ioctl.h>
