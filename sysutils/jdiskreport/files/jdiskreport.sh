#! /bin/sh
# $FreeBSD: ports/sysutils/jdiskreport/files/jdiskreport.sh,v 1.2 2003/12/09 01:54:11 linimon Exp $
JDISKDIR="%%JDISKDIR%%"
JAVAVM="%%JAVAVM%%"
PORTVERSION="%%PORTVERSION%%"

$JAVAVM -jar "$JDISKDIR/jdiskreport-$PORTVERSION.jar"
