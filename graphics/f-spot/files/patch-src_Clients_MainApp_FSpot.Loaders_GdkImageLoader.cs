
$FreeBSD: ports/graphics/f-spot/files/patch-src_Clients_MainApp_FSpot.Loaders_GdkImageLoader.cs,v 1.1 2011/06/12 19:06:22 romain Exp $

--- src/Clients/MainApp/FSpot.Loaders/GdkImageLoader.cs.orig
+++ src/Clients/MainApp/FSpot.Loaders/GdkImageLoader.cs
@@ -112,7 +112,7 @@
 		public new bool Close ()
 		{
 			lock (sync_handle) {
-				return base.Close (true);
+				return base.Close ();
 			}
 		}
 #endregion
