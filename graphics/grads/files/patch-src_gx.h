$FreeBSD: ports/graphics/grads/files/patch-src_gx.h,v 1.1 2005/04/07 21:48:51 glewis Exp $

--- src/gx.h.orig	Fri Mar 25 15:37:16 2005
+++ src/gx.h	Fri Mar 25 15:37:53 2005
@@ -45,7 +45,7 @@
 /* Default directory containing the stroke and map data sets.
    User can override this default via setenv GADDIR */
 
-static char *datad = "/usr/local/lib/grads";
+static char *datad = "%%DATADIR%%";
 
 /* Option flag.  If 0, map data set is only read once into a
    dynamically allocated memory area.  The memory is held onto
