$FreeBSD: ports/graphics/grads/files/patch-src_gagui.c,v 1.1 2005/04/07 21:48:51 glewis Exp $

--- src/gagui.c.orig	Fri Mar 25 15:20:19 2005
+++ src/gagui.c	Fri Mar 25 15:20:47 2005
@@ -37,8 +37,8 @@
 
 #include <time.h>
 
-#include "libsx.h"		
-#include "freq.h"	
+#include "X11/libsx/libsx.h"		
+#include "X11/libsx/freq.h"	
 #include "grads.h"
 #include "gx.h"
 
