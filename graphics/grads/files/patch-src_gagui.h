$FreeBSD: ports/graphics/grads/files/patch-src_gagui.h,v 1.1 2005/04/07 21:48:51 glewis Exp $

--- src/gagui.h.orig	Fri Mar 25 15:17:42 2005
+++ src/gagui.h	Fri Mar 25 15:18:16 2005
@@ -11,7 +11,7 @@
  */
 /* kk --- 020619 added List and Free_List --- kk */
 
-#include "libsx.h"
+#include "X11/libsx/libsx.h"
 
 int init_display(int argc, char **argv, void *data);
 int Custom_GUI( char *fname );
