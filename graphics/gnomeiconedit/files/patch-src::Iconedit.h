
$FreeBSD: ports/graphics/gnomeiconedit/files/patch-src::Iconedit.h,v 1.2 2007/10/24 23:35:54 marcus Exp $

--- src/Iconedit.h	2001/08/31 13:21:17	1.1
+++ src/Iconedit.h	2001/08/31 13:21:43
@@ -13,7 +13,7 @@
 #endif				/* __cplusplus */
 
 /** typedefs **/
-#include <liboaf/gnome-factory.h>
+#include <libgnorba/gnome-factory.h>
 #if !defined(ORBIT_DECL_GNOME_Iconedit_Iconedit) && !defined(_GNOME_Iconedit_Iconedit_defined)
 #define ORBIT_DECL_GNOME_Iconedit_Iconedit 1
 #define _GNOME_Iconedit_Iconedit_defined 1
