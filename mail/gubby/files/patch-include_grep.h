
$FreeBSD: ports/mail/gubby/files/patch-include_grep.h,v 1.1 2006/12/27 16:20:50 mich Exp $

--- include/grep.h.orig
+++ include/grep.h
@@ -45,7 +45,7 @@
 		trig.push_back(trigger);
 	}
 
-	inline const std::string& grep::gettrigger(const int n) const {
+	inline const std::string& gettrigger(const int n) const {
 		return trig[n];
 	}
 
