
$FreeBSD: ports/mail/gubby/files/patch-include_guess.h,v 1.1 2006/12/27 16:20:50 mich Exp $

--- include/guess.h.orig
+++ include/guess.h
@@ -38,7 +38,7 @@
 public:
 	guess();
 
-	inline const std::string guess::str() const {
+	inline const std::string str() const {
 		return procmaillog;
 	}
 
