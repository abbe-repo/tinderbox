
$FreeBSD: ports/mail/gubby/files/patch-include_initcurs.h,v 1.1 2006/12/27 16:20:50 mich Exp $

--- include/initcurs.h.orig
+++ include/initcurs.h
@@ -41,7 +41,7 @@
 	initcurs();
 	~initcurs();
 
-	inline bool initcurs::hascolors() const {
+	inline bool hascolors() const {
 		return colors;
 	}
 
