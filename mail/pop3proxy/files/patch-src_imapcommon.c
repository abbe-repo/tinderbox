
$FreeBSD: ports/mail/pop3proxy/files/patch-src_imapcommon.c,v 1.1 2006/11/23 17:36:33 pav Exp $

--- src/imapcommon.c.orig
+++ src/imapcommon.c
@@ -121,6 +121,7 @@
 #include <errno.h>
 
 #include <openssl/evp.h>
+#include <openssl/md5.h>
 
 #include <pthread.h>
 #include <sys/types.h>
