#!/bin/sh
#
# $FreeBSD: ports/misc/pauker/files/pauker.sh,v 1.1 2007/12/14 23:42:40 miwi Exp $

"%%LOCALBASE%%/bin/java" -jar "%%JAVAJARDIR%%/%%PORTNAME%%.jar" "$@"
