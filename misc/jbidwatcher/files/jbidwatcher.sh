#!/bin/sh
#
# $FreeBSD: ports/misc/jbidwatcher/files/jbidwatcher.sh,v 1.2 2005/02/01 00:59:11 hq Exp $

"%%LOCALBASE%%/bin/java" -jar "%%JAVAJARDIR%%/jbidwatcher.jar" "$@"
