
$FreeBSD: ports/misc/ytree/files/patch-ytree.h,v 1.1 2006/01/07 10:35:53 olgeni Exp $

--- ytree.h.orig
+++ ytree.h
@@ -14,6 +14,7 @@
 #include <stdio.h>
 #include <ctype.h>
 #include <math.h>
+#include <locale.h>
 
 #ifdef XCURSES
 #include <xcurses.h>
